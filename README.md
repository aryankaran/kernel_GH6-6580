# Nougat Kernel Source ported from Walton Primo Gh6 7.1.1 Kernel to Intex Cloud Q11

# Status

Unknown : Touch not working. Reboots after some time.

# Info
Defcofig : gh6_mt6580_defconfig<br />
build script : Build.sh<br />

Chipset : MT6580<br />
Kernel  : 3.18.35<br />
Android : 7.0/7.1.1<br />
